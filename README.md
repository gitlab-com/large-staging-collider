# README

The "Large Staging Collider" (LSC) is used for automated load testing of
GitLab's staging environment. The name comes from the [Large Hadron
Collider](https://en.wikipedia.org/wiki/Large_Hadron_Collider).

# How it works

Load testing is done by simply running [siege](https://github.com/JoeDog/siege)
on a bunch of URLs at a regular interval. Monitoring of the results in turn is
done using Prometheus.

# Requirements

* siege

# Usage

Assuming siege is installed, just run the following:

    siege -c4 -d 1 -t 30m -i -f urls/anonymous.txt

This will run siege with 4 concurrent connections, with a random delay of 1
second, for 30 minutes. URLs are requested in random order.

# Adding URLs

To add a URL to test, simply add it to one of the files in `urls/`. URLs that
don't require authentication should be added to `urls/anonymous.txt`. URLs that
do require authentication should instead be added to `urls/authenticated.txt`.

Each URL should be placed on its own line. Comments can be included using a `#`.
For more information on the file format, refer to [The URLs
File](https://www.joedog.org/siege-manual/#a05).
