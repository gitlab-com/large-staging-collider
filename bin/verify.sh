#!/usr/bin/env bash

set -e

# This script performs a simple check to see if the USER_TOKEN variable still
# points to a valid access token.

function verify {
    local status

    echo 'Checking status of the access token...'

    status=$(
        curl -s -o /dev/null -w '%{http_code}' \
            --header "Private-Token: $USER_TOKEN" \
            'https://staging.gitlab.com/dashboard/todos'
    )

    if [[ "$status" == "200" ]]
    then
        echo "HTTP 200: the token is valid"
        exit 0
    else
        echo "HTTP $status: it seems the token is no longer valid"
        exit 1
    fi
}

verify
