#!/usr/bin/env bash

# Runs siege twice, once to measure requests without authentication, and once to
# measure requests with authentication.

set -e

DURATION_TO_USE="${DURATION:-15m}"
CONCURRENT_USERS="${USERS:-4}"
TIME_DELAY="${DELAY:-1}"

USAGE="
Usage: [DURATION,USERS,DELAY = ?] bench.sh
Variables:
  DURATION:  The duration to run siege.  Examples: 1m, 30s, 2h
  DELAY:     Time DELAY, random delay before each request
  USERS:     Number of concurrent users
----------------------------------------------
Description:
  Benchmark the staging server using Siege
"

if [[ $# -ne 0 ]]
then
    echo "$USAGE"
    exit 1
fi

if [[ -z "$USER_TOKEN" ]]
then
    echo 'USER_TOKEN not specified.
          Only running anonymously. (Specify USER_TOKEN to also run against authenticated URLs)'
else
    echo 'USER_TOKEN specified. Running anonymous and authenticated.'
fi

function run {
    local duration
    local concurrency
    local delay

    duration="$DURATION_TO_USE"
    concurrency="$CONCURRENT_USERS"
    delay="$TIME_DELAY"

    echo "***** Siege Started ($(date -u)) with options *****"
    echo "==================="
    echo "DURATION : ${duration}"
    echo "USERS    : ${concurrency} "
    echo "DELAY    : ${delay}"
    echo "==================="

    siege -c${concurrency} -d${delay} -t ${duration} -i -f urls/anonymous.txt --no-parser

    if [[ "$USER_TOKEN" ]]
    then
      siege -c${concurrency} -d${delay} -t ${duration} -i -f urls/authenticated.txt \
          --header "Private-Token: $USER_TOKEN" \
          --no-parser
    fi
    echo "***** Siege Complete ($(date -u)) *****"
}

run
